package mimosa.table

import scala.collection.mutable.ArrayBuffer

class Table:
  val rows = new ArrayBuffer[Row]
  def add(r: Row): Unit = rows += r
  override def toString = rows.mkString("Table(", ", ", ")")

class Row:
  val cells = new ArrayBuffer[Cell]
  def add(c: Cell): Unit = cells += c
  override def toString = cells.mkString("Row(", ", ", ")")

case class Cell(elem: String)

object Table:
  // context function
  def table(init: Table ?=> Unit) =
    given t: Table = Table()
    init
    t

  def row(init: Row ?=> Unit)(using t: Table) =
    given r: Row = Row()
    init
    t.add(r)

  def cell(str: String)(using r: Row) =
    r.add(new Cell(str))

  @main def bla =
    val t = table {
      row {
        cell("A0")
        cell("B0")
      }
      row {
        cell("A1")
        cell("B1")
      }
    }
    println(t)
