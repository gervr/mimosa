package mimosa.matrix

import compiletime.ops.any.==
import compiletime.ops.int.*
import compiletime.ops.boolean.*
import compiletime.constValue
import compiletime.*

type Dim[N <: Int] = N match
  case N >= 0 => N

type Size[Xs <: Tuple, N <: Int] = Xs match
  case EmptyTuple => N
  case x *: xs => Size[xs, N + 1]

type Size0[Xs <: Tuple] = Size[Xs, 0]

type Size3 = [Xs <: Tuple] =>> Xs match
  case EmptyTuple => 0
  case x *: xs => S[Size3[xs]]

class ColVector[N <: Int](val array: Array[Double]):

  inline def size: Int = valueOf[N]

  inline def mul(a: Double): ColVector[N] =
    val newArray = Array.ofDim[Double](size)
    ColVector.update(newArray, i => a * array(i))
    new ColVector[N](newArray)

  inline def +(v: ColVector[N]): ColVector[N] =
    ColVector[N](i => array(i) + v.array(i))

  def apply(i: Int): Double = array(i)

  inline def apply[I <: Int]: Double = array(constValue[I])

  inline def x: Double = apply[0]

  inline def show: String =
    s"Vec[$size](${array.mkString(", ")})"

  inline override def toString: String =
    s"Vec[${array.length}](${array.mkString(", ")})"

object ColVector:

  transparent inline def zero[N <: Int]: ColVector[N] =
    new ColVector[N](createArray[N])

  inline def show[N <: Int](inline v: ColVector[N]): String = s"bla ${constValue[N]}"

  transparent inline def bla(inline a: Tuple): ColVector[?] = ???

  private inline def createArray[N <: Int]: Array[Double] =
    inline if constValue[N] < 0 then error("Dimension should be >= 0")
    Array.ofDim[Double](constValue[N])

  private inline def createArray2[N <: Int](using N >= 0 =:= true): Array[Double] =
    Array.ofDim[Double](constValue[N])

  private inline def createArray3[N <: Int & N > 0]: Array[Double] =
    Array.ofDim[Double](constValue[N])

  inline def one[N <: Int]: ColVector[N] =
    val array = createArray[N]
    update[N](array, _ => 1.0)
    new ColVector[N](array)

  inline def apply[N <: Int](inline f: Int => Double): ColVector[N] =
    val array = createArray[N]
    update[N](array, f)
    new ColVector[N](array)

  private inline def set[N <: Int](inline array: Array[Double], c: Double): Unit =
    var i = 0
    while i < constValue[N] do
      array(i) = c
      i += 1

  private inline def update[N <: Int](inline array: Array[Double], inline f: Int => Double): Unit =
    var i = 0
    while i < constValue[N] do
      array(i) = f(i)
      i += 1
