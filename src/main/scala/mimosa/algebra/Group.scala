package mimosa.algebra

trait Group[A] extends Monoid[A]:
  extension (x: A)
    def inverse: A
