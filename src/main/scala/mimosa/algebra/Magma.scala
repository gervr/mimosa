package mimosa.algebra

trait Magma[A]:
  extension (x: A) infix def combine(y: A): A
