package mimosa.algebra

trait Semigroup[A] extends Magma[A]

object Semigroup:

  object Laws:
    def associative[A](x: A, y: A, z: A)(using Semigroup[A]): Boolean =
      (x combine (y combine z)) == ((x combine y) combine z)
