package mimosa.algebra

trait Monad[F[_]] extends Functor[F]:

  def pure[A](x: A): F[A]

  extension [A](x: F[A])
    def flatMap[B](f: A => F[B]): F[B]

    def map[B](f: A => B) = x.flatMap(f.andThen(pure))

end Monad

object Monad:

  given listMonad: Monad[List] with

    def pure[A](x: A): List[A] =
      List(x)

    extension [A](xs: List[A])
      def flatMap[B](f: A => List[B]): List[B] =
        xs.flatMap(f)

  end listMonad
  
  given readerMonad[Ctx]: Monad[[X] =>> Ctx => X] with

    def pure[A](x: A): Ctx => A =
      ctx => x

    extension [A](x: Ctx => A)
      def flatMap[B](f: A => Ctx => B): Ctx => B =
        ctx => f(x(ctx))(ctx)

  end readerMonad

end Monad