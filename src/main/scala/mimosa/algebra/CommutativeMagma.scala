package mimosa.algebra

trait CommutativeMagma[A] extends Magma[A]

object CommutativeMagma:

  object Laws:
    def commutative[A](x: A, y: A)(using CommutativeMagma[A]): Boolean =
      (x combine y) == (y combine x)
