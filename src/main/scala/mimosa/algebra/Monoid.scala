package mimosa.algebra

trait Monoid[A] extends Semigroup[A], Unital[A]
