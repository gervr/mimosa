package mimosa.algebra

trait Ring[A, B] extends Group[A]:
  def times: Monoid[A]