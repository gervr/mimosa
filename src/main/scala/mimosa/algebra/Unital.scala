package mimosa.algebra

trait Unital[A]:
  def unit[A]: A

object Unital:

  trait Laws[A]:

    def leftIdentity(x: A)(using M: Magma[A])(using U: Unital[A]): Boolean =
      (summon[Unital[A]].unit[A] combine x) == x

    def rightIdentity(x: A)(using U: Unital[A], M: Magma[A]): Boolean =
      (x combine U.unit[A]) == x
