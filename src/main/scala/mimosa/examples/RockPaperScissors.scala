package mimosa.examples

import mimosa.algebra.CommutativeMagma

enum RockPaperScissors:
  case Rock, Paper, Scissors

object RockPaperScissors:

  given CommutativeMagma[RockPaperScissors] with
    extension (x: RockPaperScissors)
      infix def combine(y: RockPaperScissors): RockPaperScissors =
        x match {
          case Rock => if y == Scissors then x else y
          case Paper => if y == Rock then x else y
          case Scissors => if y == Paper then x else y
        }

  @main def rps(): Unit =
    import CommutativeMagma.Laws.commutative

    def show(x: RockPaperScissors, y: RockPaperScissors): Unit = {
      val commutativity = if commutative(x, y) then "holds" else "does not hold"
      println(
        s"$x vs $y: ${x combine y} wins, and commutativity $commutativity"
      )
    }

    show(Rock, Rock)
    show(Rock, Paper)
    show(Rock, Scissors)
    show(Paper, Rock)
    show(Paper, Paper)
    show(Paper, Scissors)
    show(Scissors, Rock)
    show(Scissors, Paper)
    show(Scissors, Scissors)

    def show2(x: RockPaperScissors, y: RockPaperScissors, z: RockPaperScissors): Unit =
      println(s"default order is left associative: $x vs $y vs $z = ${x combine y combine z}")
      println(s"($x vs $y) vs $z = ${(x combine y) combine z}")
      println(s"$x vs ($y vs $z) = ${x combine (y combine z)}")
      println()

    println()
    show2(Rock, Paper, Scissors)
    show2(Paper, Scissors, Rock)
    show2(Scissors, Rock, Paper)
