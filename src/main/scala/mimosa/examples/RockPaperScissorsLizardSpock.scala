package mimosa.examples

import mimosa.algebra.CommutativeMagma

enum RockPaperScissorsLizardSpock:
  case Rock, Paper, Scissors, Lizard, Spock

object RockPaperScissorsLizardSpock:

  given CommutativeMagma[RockPaperScissorsLizardSpock] with
    extension (x: RockPaperScissorsLizardSpock)
      infix def combine(y: RockPaperScissorsLizardSpock): RockPaperScissorsLizardSpock =
        x match {
          case Rock => if y == Scissors || y == Lizard then x else y
          case Paper => if y == Rock || y == Spock then x else y
          case Scissors => if y == Paper || y == Lizard then x else y
          case Lizard => if y == Paper || y == Spock then x else y
          case Spock => if y == Rock || y == Scissors then x else y
        }

  @main def rpsls(): Unit =
    import CommutativeMagma.Laws.commutative

    def show(x: RockPaperScissorsLizardSpock, y: RockPaperScissorsLizardSpock): Unit = {
      val commutativity = if commutative(x, y) then "holds" else "does not hold"
      println(
        s"$x vs $y: ${x combine y} wins, and commutativity $commutativity"
      )
    }

    show(Rock, Rock)
    show(Rock, Paper)
    show(Rock, Scissors)
    show(Rock, Lizard)
    show(Rock, Spock)
    show(Paper, Paper)
    show(Paper, Scissors)
    show(Paper, Lizard)
    show(Paper, Spock)
    show(Scissors, Scissors)
    show(Scissors, Lizard)
    show(Scissors, Spock)
    show(Lizard, Lizard)
    show(Lizard, Spock)
    show(Spock, Spock)
