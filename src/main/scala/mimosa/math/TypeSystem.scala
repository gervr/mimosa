package mimosa.math

import scala.compiletime.ops.int.*

type Bounded[Min <: Int, Max <: Int] = Max match
  case Min => Min
  case ? => Max | Bounded[Min, Max - 1]

type GCD[A <: Int, B <: Int] <: Int = B match {
  case 0 => A
  case ? => GCD[B, A % B]
}

type FracOrIntegral[A, B] = B match
  case 1 => A
  case ? => (A, B)

type SimplifyFrac[A <: Int, B <: Int, N <: Int] = FracOrIntegral[A / N, B / N]

type Frac[A <: Int, B <: Int] = SimplifyFrac[A, B, GCD[A, B]]

val a: Bounded[1, 4] = 3
val b: GCD[7, 3] = 1

val c: Frac[3, 4] = (3, 4)
val d: Frac[15, 5] = 3

val e: Frac[6, 8] = (3, 4)
// val f: Frac[3, 4] = (6, 8) // does not compile
