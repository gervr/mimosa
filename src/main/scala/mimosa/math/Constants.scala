package mimosa.math

import compiletime.ops.*

inline val π = 3.14

inline val pie = "🥧"

inline val pi: 3.14 = 3.14

inline val mpi = math.Pi

inline val tenth: 0.1 = 1e-1

@main def whatever() =
  val twoπ = π + π // while be replaced with "val twoπ = 6.28" by 'constant folding'
  val twoPies = pie + pie
  println(s"> 2π = $twoπ")
  println(s"> two pies = ${twoPies}")
