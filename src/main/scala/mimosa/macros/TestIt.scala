package mimosa.macros

object TestIt:

  import Debug.*

  hello()

  val z = 2

  def test =
    val x = 0
    val y = 1

    println("--")

    debugSingle(x)
    debugSingle(x + y)

    println("--")

    debug(x)
    debug(x, y)
    debug(x + y)
    debug(x, x + y)
    debug("A", x, x + y)
    debug("A", x, "B", y)
    debug(x, y, z)

  @main def justdoit(): Unit =
    test
