lazy val root = project
  .in(file("."))
  .settings(
    name := "mimosa",
    version := "0.0.0",
    scalaVersion := "3.0.0",
    scalacOptions ++= Seq(
      "-deprecation",
      "-feature",
      "-source:future",
      "-unchecked"
    ),
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % "3.2.9" % Test
    )
  )
